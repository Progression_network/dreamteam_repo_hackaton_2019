<?php  session_start(); if(isset($_SESSION['uname'])){ ?>
<!DOCTYPE html>
<html class="no-js before-run" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Дримтим">
    <meta name="author" content="Redixbit Sofware Technology">
    <title>Панель парнера | Добавить кладбище</title>
    <link rel="apple-touch-icon" href="uploads/logo.png">
    <link rel="shortcut icon" href="uploads/logo.png">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/bootstrap-extend.min.css">
    <link rel="stylesheet" href="assets/css/site.min.css">
    <link rel="stylesheet" href="assets/vendor/animsition/animsition.css">
    <link rel="stylesheet" href="assets/vendor/asscrollable/asScrollable.css">
    <link rel="stylesheet" href="assets/vendor/switchery/switchery.css">
    <link rel="stylesheet" href="assets/vendor/intro-js/introjs.css">
    <link rel="stylesheet" href="assets/vendor/slidepanel/slidePanel.css">
    <link rel="stylesheet" href="assets/vendor/flag-icon-css/flag-icon.css">
    <link rel="stylesheet" href="assets/vendor/formvalidation/formValidation.css">
    <link rel="stylesheet" href="assets/fonts/web-icons/web-icons.min.css">
    <link rel="stylesheet" href="assets/fonts/brand-icons/brand-icons.min.css">
    <script src="js/jquery.min.js"></script>

    <style>
        .has-feedback .form-control-feedback {
            right: 15px;
        }
        .summary-errors p,
        .summary-errors ul li a {
            color: inherit;
        }

        .summary-errors ul li a:hover {
            text-decoration: none;
        }

        @media (min-width: 768px) {
            #exampleFullForm .form-horizontal .control-label {
                text-align: inherit;
            }
        }
    </style>
    <script src="assets/vendor/html5shiv/html5shiv.min.js"></script>
    <script src="assets/vendor/media-match/media.match.min.js"></script>
    <script src="assets/vendor/respond/respond.min.js"></script>
    <script src="assets/vendor/modernizr/modernizr.js"></script>
    <script src="assets/vendor/breakpoints/breakpoints.js"></script>
    <script> Breakpoints(); </script>
</head>
<body>
<?php
include'include/header.php';
include'include/rightside.php';
include 'include/center.php';
include'include/footer.php';
?>

<script>
    $(document).ready(function() {
        $('#photoimg').die('click').live('change', function()			{
            $("#imageform").ajaxForm({target: '#preview',
                beforeSubmit:function(){
                    console.log('ttest');
                    $("#imageloadstatus").show();
                    $("#imageloadbutton").hide();
                },
                success:function(){
                    console.log('test');
                    $("#imageloadstatus").hide();
                    $("#imageloadbutton").show();
                },
                error:function(){
                    console.log('xtest');
                    $("#imageloadstatus").hide();
                    $("#imageloadbutton").show();
                } }).submit();
        });
    });
</script>
<script src="http://maps.google.com/maps/api/js?libraries=places&region=india&language=en&sensor=true"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
        <script src="assets/vendor/jquery/jquery.js"></script>
        <script src="assets/vendor/bootstrap/bootstrap.js"></script>
        <script src="assets/vendor/animsition/jquery.animsition.js"></script>
        <script src="assets/vendor/asscroll/jquery-asScroll.js"></script>
        <script src="assets/vendor/mousewheel/jquery.mousewheel.js"></script>
        <script src="assets/vendor/asscrollable/jquery.asScrollable.all.js"></script>
        <script src="assets/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
        <script src="assets/vendor/switchery/switchery.min.js"></script>
        <script src="assets/vendor/intro-js/intro.js"></script>
        <script src="assets/vendor/screenfull/screenfull.js"></script>
        <script src="assets/vendor/slidepanel/jquery-slidePanel.js"></script>
        <script src="assets/vendor/formvalidation/formValidation.min.js"></script>
        <script src="assets/vendor/formvalidation/framework/bootstrap.min.js"></script>
        <script src="assets/js/core.js"></script>
        <script src="assets/js/site.js"></script>
        <script src="assets/js/sections/menu.js"></script>
        <script src="assets/js/sections/menubar.js"></script>
        <script src="assets/js/sections/sidebar.js"></script>
        <script src="assets/js/configs/config-colors.js"></script>
        <script src="assets/js/configs/config-tour.js"></script>
        <script src="assets/js/components/asscrollable.js"></script>
        <script src="assets/js/components/animsition.js"></script>
        <script src="assets/js/components/slidepanel.js"></script>
        <script src="assets/js/components/switchery.js"></script>
<script>
    (function(document, window, $) {
        'use strict';
        var Site = window.Site;
        $(document).ready(function($) {
            Site.run();
        });-
            (function() {
                $('#exampleFullForm').formValidation({
                    framework: "bootstrap",
                    button: {
                        selector: '#validateButton1',
                        disabled: 'disabled'
                    },
                    icon: null,
                    fields: {
                        username: {
                            validators: {
                                notEmpty: {
                                    message: 'Требуется название кладбища'
                                }

                            }
                        },
                        email: {
                            validators: {
                                notEmpty: {
                                    message: 'Требуется емайл адрес'
                                },
                                emailAddress: {
                                    message: 'Неверный емаейл адрес'
                                }
                            }
                        },
                        password: {
                            validators: {
                                notEmpty: {
                                    message: 'Требуется ввод пароля'
                                },
                                stringLength: {
                                    min: 8
                                }
                            }
                        },

                        zipcode: {
                            validators: {
                                notEmpty:{
                                    message: 'Требуется ввод почтового кода'
                                },
                                digits : {
                                    message: 'Доступны только цифровые значения'
                                }
                            }
                        },

                        mobile: {
                            validators: {
                                notEmpty: {
                                    message: 'Требуется номер телефона'
                                },
                                digits : {
                                    message: 'Требуется номер телефона'
                                }

                            }
                        },

                        birthday: {
                            validators: {
                                notEmpty: {
                                    message: 'Требуется дата рождения'
                                },
                                date: {
                                    format: 'YYYY/MM/DD'
                                }
                            }
                        },
                        latitude: {
                            validators: {
                                notEmpty: {
                                    message: 'Требуется долгота'
                                }

                            }
                        },

                        longitude: {
                            validators: {
                                notEmpty: {
                                    message: 'Требуется широта'
                                }
                            }
                        },
                        time: {
                            validators: {
                                notEmpty: {
                                    message: 'Требуется время'
                                },
                                time : {
                                    message: 'Доступны только цифры'
                                }
                            }
                        },
                        time2: {
                            validators: {
                                notEmpty: {
                                    message: 'Требуется время до'
                                },
                                time : {
                                    message: 'Доступны только цифры'
                                }
                            }
                        },

                        'photos[]': {
                            validators: {
                                notEmpty: {
                                    message: 'Пожалуйста выберите изображение'
                                }
                            }
                        },
                        'thumbs': {
                            validators: {
                                notEmpty: {
                                    message: 'Пожалуйста выберите изображение'
                                }
                            }
                        },
                        porto_is: {
                            validators: {
                                notEmpty: {
                                    message: 'Пожалуйста выберите хотя бы один'
                                }
                            }
                        },
                        'for[]': {
                            validators: {
                                notEmpty: {
                                    message: 'Пожалуйста выберите хотя бы один'
                                }
                            }
                        },
                        company: {
                            validators: {
                                notEmpty: {
                                    message: 'Пожалуйста введите имя компании'
                                }
                            }
                        },
                        browsers: {
                            validators: {
                                notEmpty: {
                                    message: 'Пожалуйста укажите хотя бы одно место!'
                                }
                            }
                        }
                    }
                });
            })();
        (function() {
            $('#exampleConstraintsForm, #exampleConstraintsFormTypes').formValidation({
                framework: "bootstrap",
                icon: null,
                fields: {
                    type_email: {
                        validators: {
                            emailAddress: {
                                message: 'Не действительный email адрес'
                            }
                        }
                    },
                    type_url: {
                        validators: {
                            url: {
                                message: 'Ссылка не действительна'
                            }
                        }
                    },
                    type_digits: {
                        validators: {
                            digits: {
                                message: 'Введите только цифры'
                            }
                        }
                    },
                    type_numberic: {
                        validators: {
                            integer: {
                                message: 'Введены недопустимые значения'
                            }
                        }
                    },
                    type_phone: {
                        validators: {
                            phone: {
                                message: 'Введите актуальный номер телефона'
                            }
                        }
                    },
                    type_credit_card: {
                        validators: {
                            creditCard: {
                                message: 'Неверный формат кредитной карты'
                            }
                        }
                    },
                    type_date: {
                        validators: {
                            date: {
                                format: 'Год/месяц/день'
                            }
                        }
                    },
                    type_color: {
                        validators: {
                            color: {
                                type: ['hex', 'hsl', 'hsla', 'keyword', 'rgb',
                                    'rgba'
                                ], // The default value for type
                                message: 'Введите актуальный цвет'
                            }
                        }
                    },
                    type_ip: {
                        validators: {
                            ip: {
                                ipv4: true,
                                ipv6: true,
                                message: 'ip неверный'
                            }
                        }
                    }
                }
            });
        })();
        (function() {
            $('#exampleStandardForm').formValidation({
                framework: "bootstrap",
                button: {
                    selector: '#validateButton2',
                    disabled: 'disabled'
                },
                icon: null,
                fields: {
                    standard_fullName: {
                        validators: {
                            notEmpty: {
                                message: 'Требуется полное название. Не может быть пустым.'
                            }
                        }
                    },
                    standard_email: {
                        validators: {
                            notEmpty: {
                                message: 'Недопустимый email адрес'
                            },
                            emailAddress: {
                                message: 'Недопустимый email адрес'
                            }
                        }
                    },
                    standard_content: {
                        validators: {
                            notEmpty: {
                                message: 'Контент не может быть пустым'
                            },
                            stringLength: {
                                max: 500,
                                message: 'Длинна не может быть более 500 символов'
                            }
                        }
                    }
                }
            });
        })();

        // Example Validataion Summary Mode
        // -------------------------------
        (function() {
            $('.summary-errors').hide();

            $('#exampleSummaryForm').formValidation({
                framework: "bootstrap",
                button: {
                    selector: '#validateButton3',
                    disabled: 'disabled'
                },
                icon: null,
                fields: {
                    summary_fullName: {
                        validators: {
                            notEmpty: {
                                message: 'Требуется полное имя, не может быть пустым'
                            }
                        }
                    },
                    summary_email: {
                        validators: {
                            notEmpty: {
                                message: 'Требуется емейл адрес'
                            },
                            emailAddress: {
                                message: 'Невалидный емейл адрес'
                            }
                        }
                    },
                    summary_content: {
                        validators: {
                            notEmpty: {
                                message: 'Требуется непустое значение'
                            },
                            stringLength: {
                                max: 500,
                                message: 'Не более 500 символов'
                            }
                        }
                    }
                }
            })

                .on('success.form.fv', function(e) {
                    // Reset the message element when the form is valid
                    $('.summary-errors').html('');
                })

                .on('err.field.fv', function(e, data) {
                    // data.fv     --> The FormValidation instance
                    // data.field  --> The field name
                    // data.element --> The field element
                    $('.summary-errors').show();

                    // Get the messages of field
                    var messages = data.fv.getMessages(data.element);

                    // Remove the field messages if they're already available
                    $('.summary-errors').find('li[data-field="' + data.field +
                        '"]').remove();

                    // Loop over the messages
                    for (var i in messages) {
                        // Create new 'li' element to show the message
                        $('<li/>')
                            .attr('data-field', data.field)
                            .wrapInner(
                            $('<a/>')
                                .attr('href', 'javascript: void(0);')
                                // .addClass('alert alert-danger alert-dismissible')
                                .html(messages[i])
                                .on('click', function(e) {
                                    // Focus on the invalid field
                                    data.element.focus();
                                })
                        ).appendTo('.summary-errors > ul');
                    }

                    // Hide the default message
                    // $field.data('fv.messages') returns the default element containing the messages
                    data.element
                        .data('fv.messages')
                        .find('.help-block[data-fv-for="' + data.field + '"]')
                        .hide();
                })

                .on('success.field.fv', function(e, data) {
                    // Remove the field messages
                    $('.summary-errors > ul').find('li[data-field="' + data.field +
                        '"]').remove();
                    if ($('#exampleSummaryForm').data('formValidation').isValid()) {
                        $('.summary-errors').hide();
                    }
                });
        })();
    })(document, window, jQuery);
</script>
</body>
</html>
<?php } else { ?><script>window.location='index.php';</script><?php } ?>